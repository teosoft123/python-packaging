
include vars.mk

default: all

clean: uninstall
	$(RM) -r ./dist ./*.egg-info

all: deploy install test

deploy: package
	python -m twine upload -u $(DEPLOYER_USER) -p $(DEPLOYER_PWD) --cert /opt/bg/common/conf/bgmaster-root.crt --repository-url $(PYPI_REPO) dist/*

install: venvcheck
	pip install --cert /opt/bg/common/conf/bgmaster-root.crt --index-url=$(PYPI_REPO)simple example-pkg-otsvinev==0.0.1

uninstall: venvcheck
	-pip uninstall -y example-pkg-otsvinev

test: venvcheck
	python -m example_pkg.hello

package: venvcheck
	python setup.py sdist

VENV=$(shell python ./check-venv.py)

venvcheck: $(VENV)

venv:
	$(info Virtual environment active)

system:
	$(error Activate virtual environment: run 'source <your-venv-dir>/bin/activate')

# Conditional make:
#ifeq ($(SOME_VAR),)
#$(info SOME_VAR not set!)
#all:
#else
#all: target1 target2 targetetc
#endif
