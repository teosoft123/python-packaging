## Python packaging 

### Prerequisites

Needs Python 3.7 virtual environment with twine installed

## References

* Python docs Here:

https://packaging.python.org/tutorials/packaging-projects/

* See Also:

 https://pypi.org/project/twine/
 https://github.com/pypa/twine
